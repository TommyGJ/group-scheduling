import React, { useState } from 'react'; 
import API from './../utils/API.js';

const FileInput = (props) => {
	const [fileInput, setFileInput] = useState(React.createRef());

	const handleSubmit = (event) => {
		event.preventDefault();
		alert(fileInput.current.files[0].name);
		formatFormAndPost();
	};

	const formatFormAndPost = async () => {
		let formData = new FormData();
		formData.append('file', fileInput.current.files[0]); 
		let result = await API.post('/upload_csv', formData);
		handleData(result);
	}

	const handleData = (returnData) => {
		console.log(returnData.data.best_times);
		props.timesSetter(returnData.data.best_times);
	}

	return(
		<form onSubmit = {handleSubmit}>
			<label>
				Upload File
			<input
				 type="file"
				 ref={fileInput}
			/>
			</label>
			<br />
		  <button type="submit">Submit</button>
		</form>
	);
};

export default FileInput;

import React from 'react'; 

const Availability = (props) => {

	const listForm = () => {
		if (props.times.length !== 0 ) {
		const format = props.times.map((time) => 
			{
				return (<li key={time}>{time}</li>); 
			});
			return format;
		}  
	}

	return(
		<div>
			<h3>{((props.times.length) ? "Availability:" : "" )}</h3>
			<ul>{listForm()}</ul>
		</div>
	);
};

export default Availability;

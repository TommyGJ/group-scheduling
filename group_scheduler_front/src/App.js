import React, { useState } from 'react'; 
import './assets/styles/App.css';
import FileInput from './components/FileInput.js';
import Availability from './components/Avalibility';


function App() {
  const [bestTimes, setBestTimes] = useState([]);
  
  return (
    <div>
      <div className="App">
        <FileInput times={bestTimes} timesSetter={setBestTimes} />
      </div>
      <div>
        <Availability times={bestTimes} />
      </div>
        
    </div>
  );
}

export default App;

class CsvController < ApplicationController
  def upload
    file = params["file"].tempfile
    csv = CsvParser.new(file)
    best_times = csv.find_best_times

    print(best_times)
    render json: { best_times: best_times }, status: 200
    #@csv = Csv.new(file: file);
    #print("csv = ", @csv.file)
  end

end

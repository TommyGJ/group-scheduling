class CsvParser
  def initialize(file)
    @csv = Spreadsheet.open(file)
  end

  def find_best_times
    sheet = @csv.worksheet 1
    times = []
    sheet.each 1 do |row| 
      times.push([row[1], row[0]])
    end
    ordered_times = times.sort { |a,b| b[0] <=> a[0] }
    best_times = ordered_times[0...5].map { |a| a[1] }
    best_times
  end
end
